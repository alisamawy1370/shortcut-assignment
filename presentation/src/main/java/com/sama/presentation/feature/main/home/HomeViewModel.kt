package com.sama.presentation.feature.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sama.core.utils.GlobalDispatcher
import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.usecase.*
import com.sama.domain.model.Result
import com.sama.presentation.feature.base.BaseViewModel
import com.sama.presentation.feature.main.model.ComicViewItem
import com.sama.presentation.feature.main.model.toComicDomain
import com.sama.presentation.feature.main.model.toComicViewItem
import com.sama.presentation.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getCurrentComicUseCase: GetCurrentComicUseCase,
    private val getComicByNumberUseCase: GetComicByNumberUseCase,
    private val getFavouriteComicByNumberUseCase: GetFavouriteComicByNumberUseCase,
    private val favourComicUseCase: FavourComicUseCase,
    private val unFavourComicUseCase: UnFavourComicUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel(){

    private var currentComicNumber = -1

    private val _progressVisibilityLiveData = MutableLiveData<Boolean>()
    val progressVisibilityLiveData: LiveData<Boolean> = _progressVisibilityLiveData

    private val _comicLiveData = MutableLiveData<ComicViewItem>()
    val comicViewLiveData: LiveData<ComicViewItem> = _comicLiveData

    private val _comicIsFavouriteLiveData = MutableLiveData<Boolean>()
    val comicIsFavouriteLiveData: LiveData<Boolean> = _comicIsFavouriteLiveData

    private val _failureEventLiveData = SingleLiveEvent<Pair<String?,Int>>()
    val failureEventLiveData: LiveData<Pair<String?,Int>> = _failureEventLiveData

    fun getCurrentComic(number: Int? = null){
        getComicByNumber(number ?: currentComicNumber)
    }

    fun getNextComic(){
        if (currentComicNumber >= 0) {
            getComicByNumber(currentComicNumber + 1)
        }
    }

    fun getPrevComic(){
        if (currentComicNumber > 0){
            getComicByNumber(currentComicNumber - 1)
        }
    }

    fun changeFavouriteState(){
        if(progressVisibilityLiveData.value == true) return

        viewModelScope.launch(globalDispatcher.main) {
            if (comicIsFavouriteLiveData.value == true) {
                unFavourComicUseCase.execute(UnFavourComicParams(currentComicNumber))
                handleComicFavouriteState(false)
            } else {
                _comicLiveData.value?.toComicDomain()?.let {
                    favourComicUseCase.execute(FavourComicParams(it))
                    handleComicFavouriteState(true)
                }
            }
        }
    }

    /**
     * will get comic from repository through useCase
     * in main thread since repository has its own data flow execution
     * decision
     */
    private var getComicJob: Job? = null
    private fun getComicByNumber(number:Int) {
        if (getComicJob?.isActive == true)
            getComicJob?.cancel()

        getComicJob = viewModelScope.launch(globalDispatcher.main) {
            setPageLoading(true)
            if (number < 0){
                when (
                    val result = getCurrentComicUseCase.execute()
                ) {
                    is Result.Success -> {
                        currentComicNumber = result.value.number
                        getComicByNumber(currentComicNumber)
                        return@launch
                    }
                    is Result.Failure -> {
                        handleDataFailure(result.error, number)
                    }
                }
            } else {
                when (
                    val result = getComicByNumberUseCase.execute(GetComicByNumberParams(number))
                ) {
                    is Result.Success -> {
                        currentComicNumber = result.value.number
                        handleDataSucceed(result.value)

                        val comicFavState = getFavouriteComicByNumberUseCase.execute(GetFavouriteComicParams(currentComicNumber))
                        handleComicFavouriteState(comicFavState is Result.Success && comicFavState.value != null)

                    }
                    is Result.Failure -> {
                        handleDataFailure(result.error, number)
                    }
                }
            }
            setPageLoading(false)
        }
    }

    private fun setPageLoading(isLoading: Boolean) {
        _progressVisibilityLiveData.value = isLoading
    }

    private fun handleDataSucceed(comicDomain: ComicDomain) {
        _comicLiveData.value = comicDomain.toComicViewItem()
    }

    private fun handleComicFavouriteState(isFav: Boolean) {
        _comicIsFavouriteLiveData.value = isFav
    }

    /**
     * emit error with comic number
     * in order to retry it with the current one
     */
    private fun handleDataFailure(error: String, comicNumber: Int) {
        _failureEventLiveData.value = Pair(error, comicNumber)
    }

}