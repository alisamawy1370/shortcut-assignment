package com.sama.presentation.feature.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.sama.core.utils.GlobalDispatcher
import com.sama.domain.comics.usecase.GetComicByNumberParams
import com.sama.domain.comics.usecase.GetComicByNumberUseCase
import com.sama.presentation.feature.base.BaseViewModel
import com.sama.domain.model.Result
import com.sama.presentation.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val useCase: GetComicByNumberUseCase,
    private val globalDispatcher: GlobalDispatcher
): BaseViewModel(){

    private companion object {
        const val SPLASH_DELAY_MS = 2000L
    }

    private val _navigateToMainLiveData = SingleLiveEvent<Unit>()
    val navigateToMainLiveData: LiveData<Unit> = _navigateToMainLiveData

    fun onSplashViewCreated() {
        viewModelScope.launch(globalDispatcher.io) {
            delay(SPLASH_DELAY_MS)
            withContext(globalDispatcher.main) {
                _navigateToMainLiveData.call()
            }
        }
    }

}