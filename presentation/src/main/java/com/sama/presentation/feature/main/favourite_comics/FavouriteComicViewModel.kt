package com.sama.presentation.feature.main.favourite_comics

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sama.core.utils.GlobalDispatcher
import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.usecase.*
import com.sama.presentation.feature.base.BaseViewModel
import com.sama.presentation.feature.main.model.ComicViewItem
import com.sama.presentation.feature.main.model.toComicViewItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouriteComicViewModel @Inject constructor(
    private val getAllFavouriteComicsUseCase: GetAllFavouriteComicsUseCase,
    private val unFavourComicUseCase: UnFavourComicUseCase,
    private val globalDispatcher: GlobalDispatcher
) : BaseViewModel() {

    private val _comicLiveData = MutableLiveData<ComicViewItem?>()
    val comicViewLiveData: LiveData<ComicViewItem?> = _comicLiveData

    private var allFavouritesComic: List<ComicDomain> = listOf()
    private var currentComicNumber = -1

    @InternalCoroutinesApi
    fun getAllFavouriteComics() {
        viewModelScope.launch(globalDispatcher.main) {
            getAllFavouriteComicsUseCase.executeStream()?.collect{
                handleDataSucceed(it)
            }
        }
    }

    fun unFavComic(){
        _comicLiveData.value?.number?.let { originNumber ->
            viewModelScope.launch(globalDispatcher.main) {
                unFavourComicUseCase.execute(UnFavourComicParams(originNumber))
            }
        }
    }

    fun getNextFavComic(){
        if (currentComicNumber + 1 < allFavouritesComic.size){
            currentComicNumber ++
            _comicLiveData.value = allFavouritesComic[currentComicNumber].toComicViewItem()
        }
    }

    fun getPrevFavComic(){
        if(currentComicNumber > 0
            && allFavouritesComic.size > 1){
            currentComicNumber --
            _comicLiveData.value = allFavouritesComic[currentComicNumber].toComicViewItem()
        }
    }

    private fun handleDataSucceed(comics: List<ComicDomain>) {
        allFavouritesComic = comics
        if (allFavouritesComic.isNotEmpty()) {
            if (currentComicNumber < 0) {
                currentComicNumber = 0
                _comicLiveData.value = allFavouritesComic[0].toComicViewItem()
            } else {
                allFavouritesComic.indexOfFirst {
                    it.number == (_comicLiveData.value?.number ?: -1)
                }
                    .takeIf { it < 0 }
                    ?.let {
                        currentComicNumber = 0
                        _comicLiveData.value = allFavouritesComic[0].toComicViewItem()
                    } ?: run {
                    //do nothing and stay on the current comic
                }
            }
        } else {
            currentComicNumber = -1
            _comicLiveData.value = null
        }
    }

}