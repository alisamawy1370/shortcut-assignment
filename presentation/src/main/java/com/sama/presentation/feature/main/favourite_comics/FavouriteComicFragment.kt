package com.sama.presentation.feature.main.favourite_comics

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import com.sama.presentation.R
import com.sama.presentation.feature.base.BaseFragment
import com.sama.presentation.feature.main.model.ComicViewItem
import com.sama.presentation.util.ImageLoader
import com.sama.presentation.util.extensions.observeNullSafe
import kotlinx.android.synthetic.main.fragment_favourite_comic.*
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@AndroidEntryPoint
class FavouriteComicFragment : BaseFragment<FavouriteComicViewModel>() {

    override val viewModel: FavouriteComicViewModel by viewModels()


    override fun layoutId(): Int = R.layout.fragment_favourite_comic

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModel()
        viewModel.getAllFavouriteComics()
    }

    private fun setupViews() {
        ibShare.setOnClickListener {
            viewModel.comicViewLiveData.value?.let{
                // todo link value is always returned empty in api so we hardcode it just for now
                val shareBody = "https://xkcd.com/${it.number}"
                shareComicLink(shareBody)
            }
        }

        ibFavourite.setOnClickListener {
            viewModel.unFavComic()
        }

        btnPrev.setOnClickListener {
            viewModel.getPrevFavComic()
        }

        btnNext.setOnClickListener {
            viewModel.getNextFavComic()
        }
    }

    private fun setupViewModel() {
        with(viewModel) {
            comicViewLiveData.observe(viewLifecycleOwner) { comicItem ->
                if (comicItem != null) {
                    setUpComicView(comicItem)
                    tvEmpty.visibility = View.GONE
                } else {
                    setUpEmptyView()
                    tvEmpty.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun setUpComicView(comicView: ComicViewItem) {
        with(comicView) {
            ImageLoader.load(ivComic, image)
            tvTitle.text = title

            if (transcript.isNotBlank()) {
                llDescription.visibility = View.VISIBLE
                tvDescriptionDetails.text = transcript
            } else {
                llDescription.visibility = View.GONE
                tvDescriptionDetails.text = ""
            }

            if (alt.isNotBlank()) {
                llAlt.visibility = View.VISIBLE
                tvAltDetails.text = alt
            } else {
                llAlt.visibility = View.GONE
                tvAltDetails.text = ""
            }
        }
    }

    private fun setUpEmptyView() {
        ivComic.setImageResource(0)
        tvTitle.text = ""

        llDescription.visibility = View.GONE
        tvDescriptionDetails.text = ""

        llAlt.visibility = View.GONE
        tvAltDetails.text = ""
    }

}