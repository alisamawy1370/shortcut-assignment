package com.sama.presentation.feature.main.model

import com.sama.domain.comics.model.ComicDomain

data class ComicViewItem(
    val alt: String,
    val day: String,
    val image: String,
    val link: String,
    val month: String,
    val news: String,
    val number: Int,
    val safeTitle: String,
    val title: String,
    val transcript: String,
    val year: String
)

fun ComicDomain.toComicViewItem() =
    ComicViewItem(
        alt, day, image, link, month, news, number, safeTitle, title, transcript, year
    )

fun ComicViewItem.toComicDomain() =
    ComicDomain(
        alt, day, image, link, month, news, number, safeTitle, title, transcript, year
    )