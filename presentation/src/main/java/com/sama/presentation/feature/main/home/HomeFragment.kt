package com.sama.presentation.feature.main.home

import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.sama.presentation.R
import com.sama.presentation.feature.base.BaseFragment
import com.sama.presentation.feature.main.model.ComicViewItem
import com.sama.presentation.util.ImageLoader
import com.sama.presentation.util.extensions.observeNullSafe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_search_box.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeViewModel>() {

    override val viewModel: HomeViewModel by viewModels()


    override fun layoutId(): Int = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModel()
        viewModel.getCurrentComic()
    }

    private fun setupViews() {
        ibShare.setOnClickListener {
            // todo link value is always returned empty in api so we hardcode it just for now
            val shareBody = "https://xkcd.com/${viewModel.comicViewLiveData.value?.number}"
            shareComicLink(shareBody)
        }

        ibFavourite.setOnClickListener {
            viewModel.changeFavouriteState()
        }

        btnPrev.setOnClickListener {
            viewModel.getPrevComic()
        }

        btnNext.setOnClickListener {
            viewModel.getNextComic()
        }


        edtSearch.doAfterTextChanged {
            searchComicByDigit(edtSearch.text.toString())
        }
    }

    private fun setupViewModel() {
        with(viewModel) {
            progressVisibilityLiveData.observeNullSafe(viewLifecycleOwner) {
                if (it) {
                    loadingLayout.visibility = View.VISIBLE
                } else {
                    loadingLayout.visibility = View.GONE
                }
            }
            comicViewLiveData.observeNullSafe(viewLifecycleOwner) {
                setUpComicView(it)
            }
            failureEventLiveData.observeNullSafe(viewLifecycleOwner) { pair ->
                viewFailureError(pair.first ?: getString(R.string.something_went_wrong)) {
                    viewModel.getCurrentComic(pair.second)
                }
            }
            comicIsFavouriteLiveData.observeNullSafe(viewLifecycleOwner) {
                setFavouriteStateView(it)
            }
        }
    }

    private fun viewFailureError(message: String, listener: View.OnClickListener) {
        val snackBar = Snackbar.make(
            root,
            message, Snackbar.LENGTH_LONG
        )
        snackBar.setAction(R.string.retry, listener)
        snackBar.show()
    }

    private fun setUpComicView(comicView: ComicViewItem) {
        with(comicView) {
            ImageLoader.load(ivComic, image)
            tvTitle.text = title

            if (transcript.isNotBlank()) {
                llDescription.visibility = View.VISIBLE
                tvDescriptionDetails.text = transcript
            } else {
                llDescription.visibility = View.GONE
                tvDescriptionDetails.text = ""
            }

            if (alt.isNotBlank()) {
                llAlt.visibility = View.VISIBLE
                tvAltDetails.text = alt
            } else {
                llAlt.visibility = View.GONE
                tvAltDetails.text = ""
            }
        }
    }

    /**
     * implement debounce action by coroutine's delay
     */
    var edtSearchJob : Job? = null
    private fun searchComicByDigit(input: String) {

        if (edtSearchJob?.isActive == true)
            edtSearchJob?.cancel()

        edtSearchJob = lifecycleScope.launch {
            delay(300L)

            try {
                val comicNumber = input.toInt()
                viewModel.getCurrentComic(comicNumber)
            } catch (nfe: NumberFormatException) {
                // not a valid int
            }

        }
    }

    /**
     * change favourite view state
     */
    private fun setFavouriteStateView(isFav:Boolean){
        if (isFav){
            ibFavourite.setImageResource(R.drawable.ic_favorite)
        } else {
            ibFavourite.setImageResource(R.drawable.ic_favorite_border)
        }
    }
}