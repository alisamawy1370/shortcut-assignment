package com.sama.presentation.comics.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import com.sama.core.utils.GlobalDispatcher
import com.sama.domain.comics.usecase.*
import com.sama.presentation.feature.main.home.HomeViewModel
import com.sama.presentation.feature.main.model.ComicViewItem
import com.sama.domain.model.Result
import com.sama.presentation.feature.main.model.toComicDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import android.R
import com.sama.presentation.util.getOrAwaitValue
import junit.framework.Assert.assertEquals
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.`when`


@RunWith(MockitoJUnitRunner::class)
internal class HomeViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var globalDispatcher: GlobalDispatcher

    private val getCurrentComicUseCase: GetCurrentComicUseCase = mock()
    private val getComicByNumberUseCase: GetComicByNumberUseCase = mock()
    private val getFavouriteComicByNumberUseCase: GetFavouriteComicByNumberUseCase = mock()
    private val favourComicUseCase: FavourComicUseCase = mock()
    private val unFavourComicUseCase: UnFavourComicUseCase = mock()

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setup(){
        globalDispatcher = GlobalDispatcher(
            main = Dispatchers.Unconfined,
            io = Dispatchers.Unconfined,
            default = Dispatchers.Unconfined
        )

        viewModel = HomeViewModel(
            getCurrentComicUseCase,
            getComicByNumberUseCase,
            getFavouriteComicByNumberUseCase,
            favourComicUseCase,
            unFavourComicUseCase,
            globalDispatcher
        )
    }

    @Test
    fun `should get comic on page creation`() = runBlocking{
        val observer = mock<Observer<ComicViewItem>>()
        val argumentCaptor = argumentCaptor<ComicViewItem>()

        val fakeItem = ComicViewItem("a", "b", "c", "0", "d","",100,"","","","")

        viewModel.comicViewLiveData.observeForever(observer)

        getCurrentComicUseCase.stub {
            onBlocking { execute() } doReturn Result.Success(
                fakeItem.toComicDomain()
            )
        }
        getComicByNumberUseCase.stub {
            onBlocking { execute(any()) } doReturn Result.Success(
                fakeItem.toComicDomain()
            )
        }

        getFavouriteComicByNumberUseCase.stub {
            onBlocking { execute(any()) } doReturn Result.Success(
                null
            )
        }

        viewModel.getCurrentComic()

        verify(observer, times(1)).onChanged(argumentCaptor.capture())
    }
}