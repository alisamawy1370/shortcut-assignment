package com.sama.data.features.comics.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "fav_comics",
    indices = [Index("id","number")]
)

data class FavComicLocalEntity(
    val alt: String,
    val day: String,
    val image: String,
    val link: String,
    val month: String,
    val news: String,
    val number: Int,
    val safeTitle: String,
    val title: String,
    val transcript: String,
    val year: String,
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0
) {


}