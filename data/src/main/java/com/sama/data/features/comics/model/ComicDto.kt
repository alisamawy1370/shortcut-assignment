package com.sama.data.features.comics.model

import com.google.gson.annotations.SerializedName

data class ComicDto(
    @SerializedName("alt") val alt: String,
    @SerializedName("day") val day: String,
    @SerializedName("img") val image: String,
    @SerializedName("link") val link: String,
    @SerializedName("month") val month: String,
    @SerializedName("news") val news: String,
    @SerializedName("num") val number: Int,
    @SerializedName("safe_title") val safeTitle: String,
    @SerializedName("title") val title: String,
    @SerializedName("transcript") val transcript: String,
    @SerializedName("year") val year: String
)