package com.sama.data.features.comics.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sama.data.features.comics.local.FavComicDao
import com.sama.data.features.comics.model.FavComicLocalEntity

@Database(entities = [FavComicLocalEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favComicDao(): FavComicDao
}