package com.sama.data.features.comics.remote

import com.sama.core.utils.GlobalDispatcher
import com.sama.data.features.comics.model.ComicDto
import com.sama.data.util.ApiResult
import com.sama.data.util.callAwait
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ComicRemoteDataSource @Inject constructor(
    private val getComicApiService: ComicApiService,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */

    suspend fun getCurrentComic(): ApiResult<ComicDto> {
        return withContext(globalDispatcher.io) {
            getComicApiService.getCurrentComic().callAwait { comicResponse ->
                comicResponse
            }
        }
    }

    suspend fun getComicByNumber(number:Int): ApiResult<ComicDto> {
        return withContext(globalDispatcher.io) {
            getComicApiService.getComicByNumber(number).callAwait { comicResponse ->
                comicResponse
            }
        }
    }
}