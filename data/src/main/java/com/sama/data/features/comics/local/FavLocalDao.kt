package com.sama.data.features.comics.local

import androidx.room.*
import com.sama.data.features.comics.model.FavComicLocalEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface FavComicDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favComic: FavComicLocalEntity)

    @Query("DELETE FROM fav_comics where number=:comicNumber")
    suspend fun delete(comicNumber:Int)

    @Query("SELECT * FROM fav_comics")
    suspend fun getAllFavoriteComics(): List<FavComicLocalEntity>

    @Query("SELECT * FROM fav_comics")
    fun getAllFavoriteComicsStream(): Flow<List<FavComicLocalEntity>>

    @Query("SELECT * FROM fav_comics where number=:comicNumber")
    suspend fun getFavoriteComicsByNumber(comicNumber:Int): FavComicLocalEntity?
}