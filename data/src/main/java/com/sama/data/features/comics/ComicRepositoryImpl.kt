package com.sama.data.features.comics

import com.sama.data.features.comics.local.FavoriteComicLocalDataSource
import com.sama.data.features.comics.remote.ComicRemoteDataSource
import com.sama.data.features.comics.remote.toDomainModel
import com.sama.data.features.comics.remote.toFavComicLocal
import com.sama.data.util.ApiResult
import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

/**
 * implementation for getting comic repository abstraction in
 * domain layer.
 * @see ComicRepository
 */
@Singleton
class ComicRepositoryImpl @Inject constructor(
    private val remoteDataSource: ComicRemoteDataSource,
    private val favoriteComicLocalDataSource: FavoriteComicLocalDataSource
) : ComicRepository {

    companion object {
        private const val UNKNOWN_API_EXCEPTION = "unknown api exception"
    }

    override suspend fun getCurrentComic(): Result<ComicDomain> {
        return when (val apiResult = remoteDataSource.getCurrentComic()) {
            is ApiResult.Success -> {
                val domainObject = apiResult.value.toDomainModel()
                Result.Success(domainObject)
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    override suspend fun getComicByNumber(number:Int): Result<ComicDomain> {
        return when (val apiResult = remoteDataSource.getComicByNumber(number)) {
            is ApiResult.Success -> {
                val domainObject = apiResult.value.toDomainModel()
                Result.Success(domainObject)
            }
            is ApiResult.Failure -> {
                Result.Failure(apiResult.error.message ?: UNKNOWN_API_EXCEPTION)
            }
        }
    }

    override suspend fun insertFavouriteComic(comic: ComicDomain) {
        favoriteComicLocalDataSource.insertFavouriteComic(comic.toFavComicLocal())
    }

    override suspend fun getFavouriteComicByNumber(comicNumber: Int): ComicDomain? {
        return favoriteComicLocalDataSource.getFavouriteComicByNumber(comicNumber)?.toDomainModel()
    }

    override suspend fun removeFavouriteComic(comicNumber: Int) {
        return favoriteComicLocalDataSource.removeFavouriteComic(comicNumber)
    }

    override suspend fun getAllFavouriteComics(): List<ComicDomain> {
        return favoriteComicLocalDataSource.getAllFavouriteComics().map {
            it.toDomainModel()
        }
    }

    override fun getAllFavouriteComicsStream(): Flow<List<ComicDomain>> {
        return favoriteComicLocalDataSource.getAllFavouriteComicsStream().map {
            it.map {
                it.toDomainModel()
            }
        }
    }

}