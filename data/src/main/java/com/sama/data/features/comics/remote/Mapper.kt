package com.sama.data.features.comics.remote

import com.sama.data.features.comics.model.ComicDto
import com.sama.data.features.comics.model.FavComicLocalEntity
import com.sama.domain.comics.model.ComicDomain

/**
 * mapper file for each layer could be mandatory
 * its kind of mapper hell but each layer should have it's
 * own model
 */

fun ComicDto.toDomainModel() = ComicDomain(
    alt,day,image, link, month, news, number, safeTitle, title, transcript, year
)

fun ComicDto.toFavComicLocal() = FavComicLocalEntity(
    alt,day,image, link, month, news, number, safeTitle, title, transcript, year
)

fun FavComicLocalEntity.toDomainModel() = ComicDomain(
    alt,day,image, link, month, news, number, safeTitle, title, transcript, year
)

fun ComicDomain.toFavComicLocal() = FavComicLocalEntity(
    alt,day,image, link, month, news, number, safeTitle, title, transcript, year
)