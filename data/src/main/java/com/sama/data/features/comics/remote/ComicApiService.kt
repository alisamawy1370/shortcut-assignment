package com.sama.data.features.comics.remote

import com.sama.data.NetworkConstants
import com.sama.data.features.comics.model.ComicDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ComicApiService {

    @GET(NetworkConstants.COMIC_ENDPOINT)
    fun getCurrentComic(): Call<ComicDto>

    @GET("{number}/${NetworkConstants.COMIC_ENDPOINT}")
    fun getComicByNumber(@Path("number") number:Int): Call<ComicDto>
}