package com.sama.data.features.comics.local

import com.sama.core.utils.GlobalDispatcher
import com.sama.data.features.comics.model.FavComicLocalEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FavoriteComicLocalDataSource @Inject constructor(
    private val favComicDao: FavComicDao,
    private val globalDispatcher: GlobalDispatcher
) {

    /**
     * It is better to have thread select here because each data provider knows
     * better that which thread is suitable for this data
     * and also it prevent issues in other layers.
     */
    suspend fun insertFavouriteComic(favComicLocalEntity: FavComicLocalEntity) {
        return withContext(globalDispatcher.io) {
            favComicDao.insert(favComicLocalEntity)
        }
    }

    @Throws
    suspend fun getFavouriteComicByNumber(comicNumber: Int): FavComicLocalEntity? {
        return withContext(globalDispatcher.io) {
            favComicDao.getFavoriteComicsByNumber(comicNumber)
        }
    }

    @Throws
    suspend fun removeFavouriteComic(comicNumber: Int) {
        return withContext(globalDispatcher.io) {
            favComicDao.delete(comicNumber)
        }
    }

    @Throws
    suspend fun getAllFavouriteComics(): List<FavComicLocalEntity> {
        return withContext(globalDispatcher.io) {
            favComicDao.getAllFavoriteComics()
        }
    }

    @Throws
    fun getAllFavouriteComicsStream(): Flow<List<FavComicLocalEntity>> {
        return favComicDao.getAllFavoriteComicsStream().flowOn(globalDispatcher.io)
    }
}