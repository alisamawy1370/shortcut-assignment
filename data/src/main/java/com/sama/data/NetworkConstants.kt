package com.sama.data

/**
 *  constants for network, we can use gradle fields
 *  instead when we want to have different urls for comics
 *  and development
 */
object NetworkConstants {

    const val BASE_URL = "https://xkcd.com/"
    const val COMIC_ENDPOINT = "info.0.json"


}