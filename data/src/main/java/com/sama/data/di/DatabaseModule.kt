package com.sama.data.di

import android.content.Context
import androidx.room.Room
import com.sama.data.features.comics.local.FavComicDao
import com.sama.data.features.comics.local.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * will provide Database and it's DAO objects for data layer
 */
@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "shortcut-app-db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideFavComicDao(appDatabase: AppDatabase): FavComicDao {
        return appDatabase.favComicDao()
    }
}