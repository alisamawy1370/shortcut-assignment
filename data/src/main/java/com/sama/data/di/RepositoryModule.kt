package com.sama.data.di

import com.sama.data.features.comics.ComicRepositoryImpl
import com.sama.domain.comics.repository.ComicRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

/**
 * this module will bind all domain layer abstractions to data
 * layer implementations.
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class RepositoryModule {

    @Binds
    @ActivityRetainedScoped
    abstract fun bindComicRepository(
        comicRepositoryImpl: ComicRepositoryImpl
    ): ComicRepository
}