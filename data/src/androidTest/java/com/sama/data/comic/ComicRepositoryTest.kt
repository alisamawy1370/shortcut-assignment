package com.sama.data.comic

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.sama.core.di.module.CoreModule
import com.sama.core.utils.GlobalDispatcher
import com.sama.data.di.DatabaseModule
import com.sama.data.di.NetworkModule
import com.sama.data.features.comics.ComicRepositoryImpl
import com.sama.data.features.comics.local.FavoriteComicLocalDataSource
import com.sama.data.features.comics.local.db.AppDatabase
import com.sama.data.features.comics.remote.ComicRemoteDataSource
import com.sama.data.util.ApiResult
import com.sama.data.utils.FakeServer
import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.Result
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.runBlocking
import org.junit.*
import javax.inject.Inject


@HiltAndroidTest
@UninstallModules(DatabaseModule::class, CoreModule::class, NetworkModule::class)
class ComicRepositoryTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var database: AppDatabase

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var fakeServer: FakeServer

    @Inject
    lateinit var remoteDataSource: ComicRemoteDataSource

    @Inject
    lateinit var globalDispatcher: GlobalDispatcher

    private lateinit var favComicLocalDataSource: FavoriteComicLocalDataSource

    private lateinit var comicRepository: ComicRepository

    @Before
    fun setup(){
        hiltRule.inject()
//        fakeServer.start()

        favComicLocalDataSource = FavoriteComicLocalDataSource(database.favComicDao(),globalDispatcher)
        comicRepository = ComicRepositoryImpl(remoteDataSource,favComicLocalDataSource)
    }

    @After
    fun teardown() {
        fakeServer.shutdown()
    }

    @Test
    fun requestCurrentComic_success() = runBlocking {
        // Given
        fakeServer.setHappyComicPathDispatcher()

        // When
        val currentComic = comicRepository.getCurrentComic()

        // Then
        Assert.assertTrue(currentComic is Result.Success)
    }
}