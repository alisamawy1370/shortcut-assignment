package com.sama.data.comic.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.sama.core.di.module.CoreModule
import com.sama.core.utils.GlobalDispatcher
import com.sama.data.R
import com.sama.data.di.DatabaseModule
import com.sama.data.di.NetworkModule
import com.sama.data.features.comics.local.FavoriteComicLocalDataSource
import com.sama.data.features.comics.local.db.AppDatabase
import com.sama.data.features.comics.model.ComicDto
import com.sama.data.features.comics.remote.toFavComicLocal
import com.sama.data.utils.JsonReader
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject


@HiltAndroidTest
@UninstallModules(DatabaseModule::class, CoreModule::class, NetworkModule::class)
class FavComicLocalTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var database: AppDatabase

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var globalDispatcher: GlobalDispatcher

    private lateinit var favComicLocalDataSource: FavoriteComicLocalDataSource

    @Before
    fun setup(){
        hiltRule.inject()

        favComicLocalDataSource = FavoriteComicLocalDataSource(database.favComicDao(),globalDispatcher)
    }

    @Test
    fun favoriteNewComic_success() = runBlocking{
        // Given
        val fakeComic = (gson.fromJson(JsonReader.getJson(R.raw.comic), ComicDto::class.java)).toFavComicLocal()

        // When
        favComicLocalDataSource.insertFavouriteComic(fakeComic)

        // Then
        val resultComic = favComicLocalDataSource.getFavouriteComicByNumber(fakeComic.number)
        Assert.assertEquals(fakeComic.number, resultComic!!.number)
    }

    @Test
    fun unFavouriteComic_success() = runBlocking{
        // Given
        val fakeComic = (gson.fromJson(JsonReader.getJson(R.raw.comic), ComicDto::class.java)).toFavComicLocal()
        favComicLocalDataSource.insertFavouriteComic(fakeComic)

        // When
        favComicLocalDataSource.removeFavouriteComic(fakeComic.number)

        // Then
        val resultComic = favComicLocalDataSource.getFavouriteComicByNumber(fakeComic.number)
        Assert.assertNull(resultComic)
    }
}