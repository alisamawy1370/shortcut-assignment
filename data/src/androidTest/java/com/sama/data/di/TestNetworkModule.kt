package com.sama.data.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sama.data.NetworkConstants.BASE_URL
import com.sama.data.features.comics.remote.ComicApiService
import com.sama.data.utils.FakeServer

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * this module will provide network related objects
 * mainly retrofit and it's dependencies
 */
@Module
@InstallIn(SingletonComponent::class)
class TestNetworkModule {

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideFakeServer(): FakeServer = FakeServer()

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient
            .Builder()
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(fakeServer: FakeServer, okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(fakeServer.baseEndpoint)
            .build()
    }

    @Singleton
    @Provides
    fun provideComicApiService(retrofit: Retrofit): ComicApiService {
        return retrofit.create(ComicApiService::class.java)
    }
}