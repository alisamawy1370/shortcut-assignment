package com.sama.data.utils

import com.sama.data.NetworkConstants
import com.sama.data.R
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest

class FakeServer {
  private val mockWebServer = MockWebServer()

  private val endpointSeparator = "/"
  private val currentComicPath = endpointSeparator + NetworkConstants.COMIC_ENDPOINT
  private val notFoundResponse = MockResponse().setResponseCode(404)

  val baseEndpoint
    get() = mockWebServer.url(endpointSeparator)

  fun start() {
    mockWebServer.start(8080)
  }

  fun setHappyComicPathDispatcher() {
    mockWebServer.dispatcher = object : Dispatcher() {
      override fun dispatch(request: RecordedRequest): MockResponse {
        val path = request.path ?: return notFoundResponse

        return with(path) {
          when {
            startsWith(currentComicPath) -> {
              MockResponse().setResponseCode(200).setBody(JsonReader.getJson(R.raw.comic))
            }
            else -> {
              notFoundResponse
            }
          }
        }
      }
    }
  }

  fun shutdown() {
    mockWebServer.shutdown()
  }
}