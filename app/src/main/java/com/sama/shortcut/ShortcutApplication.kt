package com.sama.shortcut

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShortcutApplication : Application() {

    //set config here
}