package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import javax.inject.Inject

/**
 * UseCase to favour comic
 * @see UseCase
 * @see Result
 */
class FavourComicUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<Unit, FavourComicParams> {

    override suspend fun execute(params: FavourComicParams?): Result<Unit> {
        params?.comicDomain?.let {
            comicRepository.insertFavouriteComic(it)
        }
        return Result.Success(Unit)
    }
}

/**
 * UseCase param for using get comic UseCase
 */
data class FavourComicParams(val comicDomain: ComicDomain) : UseCaseParam()