package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import javax.inject.Inject

/**
 * UseCase to Get All Favourite comics
 * @see UseCase
 * @see Result
 */
class GetAllFavouriteComicsUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<List<ComicDomain>, GetAllFavouriteComicsParams> {

    override suspend fun execute(params: GetAllFavouriteComicsParams?): Result<List<ComicDomain>> {
        val comic = comicRepository.getAllFavouriteComics().filter {
            it != null
        }
        return Result.Success(comic)
    }

    override fun executeStream(params: GetAllFavouriteComicsParams?): Flow<List<ComicDomain>>? {
        return comicRepository.getAllFavouriteComicsStream().filter {
            it != null
        }
    }
}

/**
 * UseCase param for using get all comic UseCase
 */
object GetAllFavouriteComicsParams : UseCaseParam()