package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import javax.inject.Inject

/**
 * UseCase to get current comic
 * @see UseCase
 * @see Result
 */
class GetCurrentComicUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<ComicDomain, GetCurrentComicParams> {

    override suspend fun execute(params: GetCurrentComicParams?): Result<ComicDomain> {
        return comicRepository.getCurrentComic()
    }
}

/**
 * UseCase param for using get current comic UseCase
 */
object GetCurrentComicParams : UseCaseParam()