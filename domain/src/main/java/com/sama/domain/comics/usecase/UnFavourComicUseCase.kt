package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import javax.inject.Inject

/**
 * UseCase to remove favourite comic
 * @see UseCase
 * @see Result
 */
class UnFavourComicUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<Unit, UnFavourComicParams> {

    override suspend fun execute(params: UnFavourComicParams?): Result<Unit> {
        params?.number?.let {
            comicRepository.removeFavouriteComic(it)
        }
        return Result.Success(Unit)
    }
}

/**
 * UseCase param for removing comic's favour UseCase
 */
data class UnFavourComicParams(val number: Int) : UseCaseParam()