package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import javax.inject.Inject

/**
 * UseCase to get current comic
 * @see UseCase
 * @see Result
 */
class GetComicByNumberUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<ComicDomain, GetComicByNumberParams> {

    companion object{
        const val DEFAULT_NUMBER = -1
    }

    override suspend fun execute(params: GetComicByNumberParams?): Result<ComicDomain> {
        return comicRepository.getComicByNumber(params?.number ?: DEFAULT_NUMBER)
    }
}

/**
 * UseCase param for using get comic UseCase
 */
data class GetComicByNumberParams(val number: Int) : UseCaseParam()