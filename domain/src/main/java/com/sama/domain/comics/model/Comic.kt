package com.sama.domain.comics.model

data class ComicDomain(
    val alt: String,
    val day: String,
    val image: String,
    val link: String,
    val month: String,
    val news: String,
    val number: Int,
    val safeTitle: String,
    val title: String,
    val transcript: String,
    val year: String
)