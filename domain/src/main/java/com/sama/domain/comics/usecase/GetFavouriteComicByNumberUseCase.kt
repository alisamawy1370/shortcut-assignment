package com.sama.domain.comics.usecase

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.comics.repository.ComicRepository
import com.sama.domain.model.usecase.UseCase
import com.sama.domain.model.usecase.UseCaseParam
import com.sama.domain.model.Result
import javax.inject.Inject

/**
 * UseCase to get favour comic by number
 * @see UseCase
 * @see Result
 */
class GetFavouriteComicByNumberUseCase @Inject constructor(
    private val comicRepository: ComicRepository
) : UseCase<ComicDomain?, GetFavouriteComicParams> {

    companion object{
        const val DEFAULT_NUMBER = -1
    }

    override suspend fun execute(params: GetFavouriteComicParams?): Result<ComicDomain?> {
        val comic = comicRepository.getFavouriteComicByNumber(params?.number ?: DEFAULT_NUMBER)
        return Result.Success(comic)
    }
}

/**
 * UseCase param for using get comic UseCase
 */
data class GetFavouriteComicParams(val number: Int) : UseCaseParam()