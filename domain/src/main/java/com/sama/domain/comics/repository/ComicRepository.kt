package com.sama.domain.comics.repository

import com.sama.domain.comics.model.ComicDomain
import com.sama.domain.model.Result
import kotlinx.coroutines.flow.Flow

/**
 * comic repository abstraction in domain layer
 * which is implemented by repository in data layer
 * app uses repository pattern
 */
interface ComicRepository {

    suspend fun getCurrentComic(): Result<ComicDomain>
    suspend fun getComicByNumber(number:Int): Result<ComicDomain>

    suspend fun insertFavouriteComic(favComicLocalEntity: ComicDomain)
    suspend fun getFavouriteComicByNumber(comicNumber: Int): ComicDomain?
    suspend fun removeFavouriteComic(comicNumber: Int)
    suspend fun getAllFavouriteComics(): List<ComicDomain>
    fun getAllFavouriteComicsStream(): Flow<List<ComicDomain>>
}